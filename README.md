# skillbox-diploma
Чтобы скопировать репозиторий к себе для работы, вам нужно следовать [этим инструкциям](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/duplicating-a-repository#mirroring-a-repository-in-another-location).

## Runtime
Приложение отвечает по 3 эндпоинтам:  
* /health - 200 ok
* /metrics - в формате метрик для prometheus, включая счётчик запросов в основной эндпоинт `skillbox_http_requests_total`
* / - основной эндпоинт, возвращающий часть запроса и генерирующий строчку лога.

## Как работать с приложением без docker:  
1. Установить golang 1.16
2. Установить зависимости:

   ```shell
   go mod download
   ```

3. Запустить тесты:
   ```shell
   go test -v ./...
   ```
4. Собрать приложение:
   ```
   GO111MODULE=on go build -o app cmd/server/app.go
   ```
5. Запустить его:
   ```shell
   ./app
   ```

## Как работать с приложением в docker:  
1. Установить docker
2. Запустить тесты
   ```shell
   ./run-tests.sh
   ```
3. Собрать:
   ```shell
   docker-compose build
   ```
   or
   ```shell
   docker build . -t skillbox/app
   ```
4. Запустить:
   ```shell
   docker-compose up
   ```
   or
   ```shell
   docker run -p8080:8080 skillbox/app
   ```
   ## Как деплоить приложение с помощью ansible:  
   1. Собрать и запушить докер-образ в dockerhub с помощью команд:
   ```shell
      docker build . -t asz2000/skillbox:latest
      docker login -u <username>
      docker push asz2000/skillbox
   ```
   2. Должна быть подготовлена инфраструктура по инструкции в репозитории https://gitlab.com/asz2000/infra.git.
   3. Из репозитория infra в каталог ansible скопировать файл hosts.
   4. В файле ansible/vars.yaml указать учетные данные для доступа к dockerhub.
   ```shell
      username: 
      password:
   ```
   5. В каталоге ansible запустить сценарий:
   ```shell
               ansible-playbook deploy_docker.yaml -b
   ```
   6. Приложение будет доступно по адресу: http://devops-cert.tk.